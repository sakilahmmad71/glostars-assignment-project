import About from '../components/About';
import Navbar from '../components/Navbar';
import '../styles/pages/homepage.scss';

const Homepage = () => {
  return (
    <>
      <Navbar />
      <About />
    </>
  )
}

export default Homepage
