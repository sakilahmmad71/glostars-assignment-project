const aboutData = [
  { title: 'Build Portfolio & Network', text: 'Become a part of international community of photographers - from begginers to professional. Share your photos, interact with amezing creators, inspires and get inspired.', icon: 'fa-globe-europe' },
  { title: 'Join Contest & Vote', text: 'Participate in one or more photography contest every week. Choose winning photos yourself and get ready fot Team Choice Voting as well.', icon: 'fa-award' },
  { title: 'Check The Blog', text: 'Want to learn more about photography? Check recent inductry news, useful tips and interesting stories prepared by Glowstars Community and Team especially for you', icon: 'fa-book-reader' },
];

export default aboutData;