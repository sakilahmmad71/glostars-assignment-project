const slideData = [
  { title: 'Hi, Welcome to Glowstars', slogan: 'Place of creativity, community, challenge & commerce.', offer: 'Free. For everyone.' },
  { title: 'Get all exciting photographs', slogan: 'Amezing photographs of nature, animals & arts.', offer: 'Accessible for everyone.' },
  { title: 'Join us at Glowstars', slogan: 'You will be surprised about new creative ideas and designs.', offer: 'Always active. For creativity.' }
];

export default slideData;
