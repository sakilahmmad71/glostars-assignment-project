import Homepage from './pages/Homepage';
import './styles/App.scss';

const App = () => {
  return (
    <div className="App">
      <Homepage />
    </div>
  )
}

export default App;
